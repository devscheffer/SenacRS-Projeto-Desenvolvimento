# Comparativo Com Sistemas Correlatos

|                                                           | DashCommand | Infocar | OBD MARY | Torque Lite | Herbie-21 |
| --------------------------------------------------------- | ----------- | ------- | -------- | ----------- | --------- |
| Controle de pressão dos pneus                             | X           | X       | X        | X           | X         |
| Controle de Quilometragem                                 | X           | X       | X        | X           | X         |
| Controle de  Consumo de combustível                       | X           | X       | X        | X           | X         |
| Controle de Troca de óleo                                 | X           | X       | X        | X           | X         |
| Controle de histórico de peças por nota fiscal            |             |         |          |             | X         |
| Controle de manutenção de refrigeração                    |             |         |          |             | X         |
| Manual virtual do carro                                   |             |         |          |             | X         |
| Chatbot do manual do carro                                |             |         |          |             | X         |
| Notificação de controle de manutenção(por e-mail ou push) |             |         |          |             | X         |
| Controle de depreciação do carro                          |             |         |          |             | X         |


|                     | DashCommand                                   | Infocar                                   | OBD MARY                                                | Torque Lite                      |
| ------------------- | --------------------------------------------- | ----------------------------------------- | ------------------------------------------------------- | -------------------------------- |
| Aspectos positivos  | Fácil utilização                              | Interface UI Agradável                    | Intuitivo                                               | É de graça                       |
| Aspectos a melhorar | Poderia pedir apenas  permissões  necessárias | Poderia possuir suporte para android AUTO | Precisa de um adaptador específico para poder funcionar | Poderia possuir suporte para IOS |
|                     |
