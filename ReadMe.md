# App: Herbie-21

## Integrantes Grupo 3

- Cassiano
- Gerson Scheffer (back-end)
- Gian Lucas
- Jonatan Goulart Maciel
- Leonardo Soares Duarte

## Visão de Produto

**Para** proprietário de automóveis

**Que** precisam do histórico detalhado do seu veículo

**O** Herbie-21

**É** app multiplataforma

**Que** facilita o acompanhamento da condição do veículo

**Diferentemente** de depender de anotações ou planilhas

**Nosso** produto permite gerenciar as informações sobre condições do seu veiculo diretamente na tela do aplicativo.

